import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MenuModule} from 'primeng/menu';
import {TabViewModule} from 'primeng/tabview';
import {FieldsetModule} from 'primeng/fieldset';
import {KeyFilterModule} from 'primeng/keyfilter';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import { QRCodeModule } from 'angularx-qrcode';
import {NgxPrintModule} from 'ngx-print';
import {SelectButtonModule} from 'primeng/selectbutton';

import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { InputTextModule } from 'primeng/inputtext';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/chips';
import { InputMaskModule } from 'primeng/inputmask';
import { InputNumberModule } from 'primeng/inputnumber';
import { DropdownModule } from 'primeng/dropdown';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import {ToastModule} from 'primeng/toast';
import {DialogModule} from 'primeng/dialog';
import { TableModule } from 'primeng/table';


import { CallApiService } from './service/call-api.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MenuModule,
    TabViewModule,
    CommonModule,
    BrowserModule,
    ButtonModule,
    HttpClientModule,
    PanelModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    AutoCompleteModule,
    ToastModule,
    CalendarModule,
    ChipsModule,
    InputMaskModule,
    InputNumberModule,
    SelectButtonModule,
    DropdownModule,
    MultiSelectModule,
    InputTextareaModule,
    CascadeSelectModule,
    ConfirmDialogModule,
    DialogModule,
    FieldsetModule,
    TableModule,
    QRCodeModule,
    NgxPrintModule,
    KeyFilterModule,
    BrowserAnimationsModule
  ],
  
  providers: [CallApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class Produit {

    constructor(

         public nomProduit: string,
         public codeProduit: string,
         public quantiteProduit: number,
         public categorieProduit: string,
         public prixUnite: number,
         public description: string
        ) {}
  
}

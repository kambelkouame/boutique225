import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as Rx from "rxjs/Rx";

import { from, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


import { IdStock } from '../service/id-stock';
import { User } from '../service/user';
import { Produit } from '../service/produit';
import { Commande } from '../service/commande';
import { Nom } from '../service/nom';

@Injectable({
  providedIn: 'root'
})
export class CallApiService {
  id :any;
  private apiURL = environment.mcore;

  constructor(private httpClient: HttpClient) { }

  public login(user : User){
    return this.httpClient.post(`${this.apiURL}/core/user/authenticate/`,user).
        pipe(
           map((data: any) => {
             return data;
           }), catchError( error => {
             return throwError( 'Erreur!' );
           })
        )
  }

  public insertProduit(produit : Produit){
    return this.httpClient.post(`${this.apiURL}/core/stock/newStock/`,produit).
        pipe(
           map((data: any) => {
             return data;
           }), catchError( error => {
             return throwError( 'Erreur!' );
           })
        )
  }


  public CreateCommande(commande : Commande){
    return this.httpClient.post(`${this.apiURL}/core/stock/commande/`,commande).
        pipe(
           map((data: any) => {
             return data;
           }), catchError( error => {
             return throwError( 'Erreur!' );
           })
        )
  }




  public getProduit(){
    return this.httpClient.get(`${this.apiURL}/core/stock/getAllproduct/`).
        pipe(
           map((data: any) => {
             return data;
           }), catchError( error => {
             return throwError( error );
           })
        )
  }

  public getProduitbyNom(nom:Nom){
    return this.httpClient.get(`${this.apiURL}/core/stock/getSockByNom/`+nom).
        pipe(
           map((data: any) => {
             return data;
    
              
           }), catchError( error => {
             return throwError( error );
           })
        )
  }

  public DeleteStock(id:IdStock){
    return this.httpClient.get(`${this.apiURL}/core/stock/deleteStock/`+ id).
        pipe(
           map((data: any) => {
             
          return data;
              
           }), catchError( error => {
             return throwError( error );
           })
        )
  }
  

}

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ConfirmationService, ConfirmEventType, MessageService } from 'primeng/api';
import { FormBuilder, FormGroup, } from '@angular/forms';
import html2canvas from "html2canvas";



import { CallApiService } from '../service/call-api.service';
import { Produit } from '../service/produit';
import { Commande} from '../service/commande';
import { IdStock } from '../service/id-stock';
import { Nom } from '../service/nom';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { jsPDF } from 'jspdf';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ConfirmationService, MessageService]
  ,
})


export class HomeComponent implements OnInit {
  nom:any;
  quantite:any;
  code:any;
  prixU:number=0
  couleur:any;
  prixttc:any;
  adresse:any;
  contact:any;
  acheteur:any;
  categorie:any;
  allProduct: any;
  nomClient: any;
  description:any;
  descriptionProduit:any;
  sommeTTc:any;
  id:any
  bonCommande:any
  nomProduit: any;

  codeProduit: any;

  quantiteProduit: any;

  categorieProduit: any;

  displayBasic2: Boolean = false;
  displayBasic3: Boolean = false;
  Boutique="ACCESSOIRES.CI";
  pays="Côte d'Ivoire";
  ville ="Abidjan";
  tel="+225 01 42 20 67 71"
  prixUnite: any;
  command:any=[];
  product: any;

newCommande:any




@ViewChild('content',{static:false}) content!: ElementRef;
@ViewChild('printSection',{static:false}) printSection!: ElementRef;

 
  constructor(
    public Api: CallApiService,
    public router: Router,
    private cookieService: CookieService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { 
 
  }

  ngOnInit(): void {

    this.getProduct();
   
  }

  
;

  printPDF(){
   /* let pdf =new jsPDF(
       'p','pt','a4');
    pdf.html(this.content.nativeElement,{
      callback:(pdf)=>{
        pdf.addImage('assets/logo-bout.png', 'PNG', 0, 10, 10, 10)
        pdf.save("CommandeBoutique225.pdf")
      }
    })*/

    html2canvas(this.content.nativeElement, {
      // Opciones
      allowTaint: true,
      useCORS: false,
      // Calidad del PDF
      scale: 1
    }).then(function(canvas) {
      var img = canvas.toDataURL("image/png");
      var doc = new jsPDF();
      doc.addImage(img,'PNG',-15,-50, 126, 175 );
      doc.save('Boutique_225.pdf');
    });
  
   
      
}


    


  confirm1() {
    this.confirmationService.confirm({
      message: 'Etes vous sure de valider ce Stock?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.insertStoct()

      },
      reject: () => {
        if (ConfirmEventType.REJECT) {
          this.messageService.add({ severity: 'error', summary: 'Confirmation', detail: 'creation du stock non abouti' });
        
        } else if (ConfirmEventType.CANCEL) {
          this.messageService.add({ severity: 'error', summary: 'Confirmation', detail: "retour" });
        
        }
      }
    });

  }


  addPanier(){
    
    
    var data = {
      'nom':this.nom,
      'quantite':this.quantite,
      'code':this.code,
      'couleur':this.couleur,
      'prixttc': this.prixttc,
      'adresse': this.adresse,
      'contact':this.contact,
      'description': this.description
        

    }
    this.bonCommande ="CI225_"+this.quantite+this.contact+"0"
    this.command.push(data);
    this.nom=""
    this.quantite=0,
    this.code="",
    this.couleur="",
    this.prixttc="",
    this.description=""
      
    this.newCommande =this.command
  }

getMontant(){
  this.sommeTTc=0
  for (let index = 0; index < this.newCommande.length; index++) {
    var data = this.newCommande[index];
   this.sommeTTc=this.sommeTTc + (data.prixttc*data.quantite)

  }
}
  valid(){
    this.addPanier();
    if(this.command.length!=0){


      for (let index = 0; index < this.command.length; index++) {
        const data = this.command[index];
       
        var commande = new  Commande(data.nom, data.quantite, data.code, data.couleur, data.prixttc,data.acheteur,data.adresse,data.contact)

       this.Commande(commande)
     
       
        
      }
      this.printPDF()
    }
      
  
    

  }


  confirm2() {
    this.confirmationService.confirm({
      message: 'Etes vous sure d\'effectuer cette commande?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        var commande = new Commande(this.nom, this.quantite, this.code, this.couleur, this.prixttc,this.acheteur,this.adresse,this.contact)

       this.Commande(commande)
      },
      reject: () => {
        if (ConfirmEventType.REJECT) {
          this.messageService.add({ severity: 'error', summary: 'Confirmation', detail: "commande non abouti" });
        
        } else if (ConfirmEventType.CANCEL) {
          this.messageService.add({ severity: 'error', summary: 'Confirmation', detail: 'retour'});
        
        }
      }
    });

  }
  showBasicDialog2() {
    this.displayBasic2 = true;
  }

  showBasicDialog3() {
    this.displayBasic3 = true;
  }


  insertStoct() {

    var produit = new Produit(this.nomProduit, this.codeProduit, this.quantiteProduit, this.categorieProduit, this.prixUnite,this.description)

    this.Api.insertProduit(produit).subscribe(
      (data: any) => {

        if (data.statut == true) {
          this.messageService.add({ severity: 'success', summary: 'Confirmation', detail: data.msg });
          location.reload()
        } else {
          this.messageService.add({ severity: 'error', summary: 'Confirmation', detail: data.msg });
        
        }

      },
      err => {
        console.warn(err)
      }
    )
  }



  getProductByNom(nom:Nom) {

    
    this.Api.getProduitbyNom(nom).subscribe(
      (data: any) => {
        console.log(data.produit[0].code)
        if (data.statut == true) {
          this.code =data.produit[0].code;
         this.prixttc = data.produit[0].prix;
        this.categorie= data.produit[0].categorie 
         this.description= data.produit[0].description 
        
             
        } else {
        
          this.messageService.add({ severity: 'error', summary: 'Confirmation', detail: "veuillez inserer de nouvau stock"});
        }

      },
      err => {
        console.warn(err)
      }
    )
  }

  Commande(commande:Commande) {

    //var commande = new Commande(this.nom, this.quantite, this.code, this.couleur, this.prixttc,this.acheteur,this.adresse,this.contact)

    this.Api.CreateCommande(commande).subscribe(
      (data: any) => {

        if (data.statut == true) {
          this.messageService.add({ severity: 'success', summary: 'Confirmation', detail: data.msg });

        } else {
          this.messageService.add({ severity: 'error', summary: 'Confirmation', detail: data.msg });
        }

      },
      err => {
        console.warn(err)
      }
    )
  }


  getProduct() {


    this.Api.getProduit().subscribe(
      (data: any) => {

        this.product =[] 
        this.product = data.product
        
        this.allProduct= this.product
        console.log(this.allProduct)
      },
      err => {
        console.warn(err)
      }
    )
  }

  GetProductInit(){

  
  }

    Delete(id:IdStock) {


    this.Api.DeleteStock(id).subscribe(
      (data: any) => {

       
        location.reload()
      },
      err => {
        console.warn(err)
      }
    )
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,} from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

import { CookieService } from 'ngx-cookie-service';
import { CallApiService } from '../service/call-api.service';
import { User } from '../service/user';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {
 


  telephone :any
  motpasse : any
  cookieValue = '';
  constructor(
    public Api: CallApiService,
    public router: Router,
    private cookieService: CookieService  ,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {

  }

  

  login() {
    var user= new User(this.telephone,this.motpasse)
  
   
    this.Api.login(user).subscribe(
      (data:any) => {

        if(data.statut==true){
          this.cookieService.set( 'telephone', this.telephone );
          this.messageService.add({ severity: 'success', summary: 'Confirmation', detail: data.msg });
        
          this.router.navigateByUrl('/home')
        }else {
          this.messageService.add({ severity: 'error', summary: 'server Msg', detail: data.msg });
        
          this.router.navigateByUrl('/login')
        }

      },
      err => {
        console.warn(err)
      }
    )
  }

 
}
